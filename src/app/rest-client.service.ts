import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { Employee } from './register/Employee';

@Injectable({
  providedIn: 'root'
})
export class RestClientService {
  constructor(private http: HttpClient) { }

  saveEmployee(employee: Employee): Observable<any> {
    return this.http.post('http://dummy.restapiexample.com/api/v1/create', employee);
  }

  getEmployees(): Observable<any> {
    return this.http.get('http://dummy.restapiexample.com/api/v1/employees');
  }
  deleteEmployee(id: number): Observable<any> {
    return this.http.delete('http://dummy.restapiexample.com/api/v1/delete/' + id);
  }
}
