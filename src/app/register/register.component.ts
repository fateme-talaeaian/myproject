import { Component, OnInit } from '@angular/core';
import {RestClientService} from '../rest-client.service';
import {Employee} from './Employee';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private restClient: RestClientService) { }

  employee: Employee;

  ngOnInit(): void {
    // @ts-ignore
    this.employee = {name: '', lastname: '', salary: null, age: null, id: null};
  }

  // tslint:disable-next-line:typedef
  saveEmployee() {

    alert("درج با موفقیت انجام شد");

    // @ts-ignore
    this.restClient.saveEmployee(this.employee).subscribe(response => {
      console.log('employee saved', response);
    });
  }


}
