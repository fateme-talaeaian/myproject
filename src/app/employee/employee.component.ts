import { Component, OnInit } from '@angular/core';
import {RestClientService} from '../rest-client.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  constructor(private restClient: RestClientService , private router: Router) { }

  employeelist = [];

  ngOnInit(): void {
    this.getAllEmployees();

  }
  // tslint:disable-next-line:typedef
  getAllEmployees() {
    this.restClient.getEmployees().subscribe(res => {
      console.log('employee list', res);
      this.employeelist = res.data;
    });
  }
  // tslint:disable-next-line:typedef
  deleteEmployee(id: number) {
    alert("حذف با موفقیت انجام شد");
    this.restClient.deleteEmployee(id).subscribe(res => {
      console.log('employee deleted', res);
    });
  }


  // tslint:disable-next-line:typedef
  navigateToRegisterComponent(){
    this.router.navigateByUrl('/register'); }

}
