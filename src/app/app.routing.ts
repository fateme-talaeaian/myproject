import {RouterModule, Routes} from '@angular/router';
import {ShowComponent} from './show/show.component';
import {NgModule} from '@angular/core';
import {RegisterComponent} from './register/register.component';
import {EmployeeComponent} from './employee/employee.component';

const routes: Routes = [
  {path: '', redirectTo: 'show', pathMatch: 'full'},
  { path: 'show' , component : ShowComponent },
  { path: 'register' , component : RegisterComponent },
  { path: 'employee' , component : EmployeeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
